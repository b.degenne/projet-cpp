#include "ClientParticulier.h"

ClientParticulier::ClientParticulier(int Ident, string Nom, string libelle, string complement, double CodePost, string ville, string mail,
                                     string prenom, string sexe, string dateNaissance)
    :Client(Ident,Nom,libelle,complement, CodePost,ville, mail)
{
    this->Setprenom(prenom);
    this->Setsexe(sexe);
    this->SetdateNaissance(dateNaissance);
}

ClientParticulier::~ClientParticulier()
{
    cout << "Destruction de (ClientParticulier): " << Getprenom() << endl;
}

void ClientParticulier::Setprenom(string prenom)
{
    /*//Verifie la longueur
    if (strlen(prenom) > 50)
    {
        strcpy(this->prenom, "?");
    }
    else
    {
        strcpy(this->prenom, prenom);
    }*/
    this->prenom = prenom;
}

string ClientParticulier::afficher(void)
{
    ostringstream oss;
    oss << Client::afficher()
        << "Prenom : " << Getprenom()
        << "\nSexe : " << Getsexe()
        << "\nDate de naissance : " << GetdateNaissance()
        << endl;
    return oss.str();
}



