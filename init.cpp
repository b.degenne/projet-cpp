#include "init.h"
#include "ClientParticulier.h"
#include "ClientPro.h"
#include <QDebug>


QMap<int, Client *> Init::mapClients;

void Init::alimenter()
{
    ClientParticulier *C1 = new ClientParticulier(1, "BETY", "12, rue des Oliviers",
              "", 94000, "CRETEIL","bety@gmail.com",
              "Daniel","M", "12/11/1985");
    ClientParticulier *C3 = new ClientParticulier(3, "BODIN", "10, rue des Olivies",
              "Etage 2", 94300, "VINCENNES","bodin@gmail.com",
              "Justin","M", "05/05/1965");
    ClientParticulier *C5 = new ClientParticulier(5, "BERRIS", "15, rue de la République",
              "", 94120, "FONTENAY SOUS BOIS","berris@gmail.com",
              "Karine","F", "06/06/1977");
    ClientParticulier *C7 = new ClientParticulier(7, "ABENIR", "25, rue de la Paix",
              "", 92100, "LA DEFENSE","abenir@gmail.com",
              "Alexandra","F", "12/04/1977");
    ClientParticulier *C9 = new ClientParticulier(9, "BENSAID", "3, avenue des Parcs",
              "", 93500, "ROISSY EN France","bensaid@gmail.com",
              "Georgia","F", "16/04/1976");
    ClientParticulier *C11 = new ClientParticulier(11, "ABABOU", "3, rue Lecourbe",
              "", 93200, "BAGNOLET","ababou@gmail.com",
              "Teddy","M", "10/10/1970");


    ClientPro *C2 = new ClientPro(2,"AXA","125, rue LaFayette","Digicode 1432",
                 94120,"FONTENAY SOUS BOIS","info@axa.fr",
                 "12548795641122","SARL",
                 "125, rue LaFayette", "Digicode 1432",
                 94120,"FONTENAY SOUS BOIS");
    ClientPro *C4 = new ClientPro(4,"PAUL","36, quai des Orfèvres","",
                 93500,"ROISSY EN France","info@paul.fr",
                 "87459564455444","EURL",
                 "10, esplanade de la Défense", "",
                 92060,"LA DEFENSE");
    ClientPro *C6 = new ClientPro(6,"AXA","125, rue LaFayette","Digicode 1432",
                 94120,"FONTENAY SOUS BOIS","info@axa.fr",
                 "12548795641122","SARL",
                 "125, rue LaFayette", "Digicode 1432",
                 94120,"FONTENAY SOUS BOIS");
    ClientPro *C8 = new ClientPro(8,"PRIMARK","32, rue E. Renan","Bat. C",
                 75002,"PARIS","info@primark.fr",
                 "08755897458455","SARL",
                 "32, rue E. Renan", "Bat. C",
                 75002,"PARIS");
    ClientPro *C10 = new ClientPro(10,"LEONIDAS","15, Place de la Bastille","Fond de Cour",
                 75003,"PARIS","contact@leonidas.fr",
                 "91235987456832","SAS",
                 "10, rue de la Paix", "",
                 75008,"PARIS");

    mapClients.insert(C1->GetIdent(), C1);
    Init::mapClients.insert(C2->GetIdent(), C2);
    Init::mapClients.insert(C3->GetIdent(), C3);
    Init::mapClients.insert(C4->GetIdent(), C4);
    Init::mapClients.insert(C5->GetIdent(), C5);
    Init::mapClients.insert(C6->GetIdent(), C6);
    Init::mapClients.insert(C7->GetIdent(), C7);
    Init::mapClients.insert(C8->GetIdent(), C8);
    Init::mapClients.insert(C9->GetIdent(), C9);
    Init::mapClients.insert(C10->GetIdent(), C10);
    Init::mapClients.insert(C11->GetIdent(), C11);


/*
    for(auto cli: mapClients.values())
    {
        qDebug() << cli->getNom();
    }
*/
}

QMap<int, Client *> Init::getMapClients()
{
    return mapClients;
}

