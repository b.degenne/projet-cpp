#include "dialog.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "ClientParticulier.h"
#include "Client.h"
#include "ClientPro.h"
#include "init.h"
#include <QDebug>
#include <QString>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QMap<int, Client *> mesClients = Init::getMapClients();

    //QString val;
    for(auto cli: mesClients.values())
    {
        int number = cli->GetIdent();
        if (number%2 == 0)
        {
            //val = val + QString::fromStdString(cli->afficher());
            ui->textClientPro->addItem(QString::fromStdString(cli->afficher()));
        }
        else
        {
            //cli->afficher();
            ui->textClientPart->addItem(QString::fromStdString(cli->afficher()));
        }
        //ui->label->setText(cli);
        //val = val + QString::number(cli->GetIdent());

        //val.append(" - ");
        //val.append(cli->GetNom());
        //val = QString::fromStdString(cli->afficher());
        //ui->cltLabel->setText(QString::fromStdString(cli->afficher()));

        //qDebug()<< QString::fromStdString(cli->GetIdent());
    }

    //Dialog* mydialog = new Dialog(this);
    //mydialog->show(); // Boite non modale
    //mydialog->exec(); // Boite modale

    //ui->textClientPart->addItem(val);
    //ui->cltLabel->setText(val);

}

void MainWindow::on_pushButton_2_clicked()
{
    Dialog* mydialog = new Dialog(this);
    //mydialog->show(); // Boite non modale
    mydialog->exec(); // Boite modale
}
