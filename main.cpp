#include "mainwindow.h"

#include "ClientParticulier.h"
#include "Client.h"
#include "ClientPro.h"
#include "init.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    Init::alimenter();

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    /*ClientParticulier C1(1, "BETY", "12, rue des Oliviers",
              "", 94000, "CRETEIL","bety@gmail.com",
              "Daniel",'M', "12/11/1985");
    ClientParticulier C3(3, "BODIN", "10, rue des Olivies",
              "Etage 2", 94300, "VINCENNES","bodin@gmail.com",
              "Justin",'M', "05/05/1965");
    ClientParticulier C5(5, "BERRIS", "15, rue de la République",
              "", 94120, "FONTENAY SOUS BOIS","berris@gmail.com",
              "Karine",'F', "06/06/1977");
    ClientParticulier C7(7, "ABENIR", "25, rue de la Paix",
              "", 92100, "LA DEFENSE","abenir@gmail.com",
              "Alexandra",'F', "12/04/1977");
    ClientParticulier C9(9, "BENSAID", "3, avenue des Parcs",
              "", 93500, "ROISSY EN France","bensaid@gmail.com",
              "Georgia",'F', "16/04/1976");
    ClientParticulier C11(11, "ABABOU", "3, rue Lecourbe",
              "", 93200, "BAGNOLET","ababou@gmail.com",
              "Teddy",'M', "10/10/1970");


    ClientPro C2(2,"AXA","125, rue LaFayette","Digicode 1432",
                 94120,"FONTENAY SOUS BOIS","info@axa.fr",
                 "12548795641122","SARL",
                 "125, rue LaFayette", "Digicode 1432",
                 94120,"FONTENAY SOUS BOIS");
    ClientPro C4(4,"PAUL","36, quai des Orfèvres","",
                 93500,"ROISSY EN France","info@paul.fr",
                 "87459564455444","EURL",
                 "10, esplanade de la Défense", "",
                 92060,"LA DEFENSE");
    ClientPro C6(6,"AXA","125, rue LaFayette","Digicode 1432",
                 94120,"FONTENAY SOUS BOIS","info@axa.fr",
                 "12548795641122","SARL",
                 "125, rue LaFayette", "Digicode 1432",
                 94120,"FONTENAY SOUS BOIS");
    ClientPro C8(8,"PRIMARK","32, rue E. Renan","Bat. C",
                 75002,"PARIS","info@primark.fr",
                 "08755897458455","SARL",
                 "32, rue E. Renan", "Bat. C",
                 75002,"PARIS");
    ClientPro C10(10,"LEONIDAS","15, Place de la Bastille","Fond de Cour",
                 75003,"PARIS","contact@leonidas.fr",
                 "91235987456832","SAS",
                 "10, rue de la Paix", "",
                 75008,"PARIS");

    C1.afficherCltPart();
    C3.afficherCltPart();
    C5.afficherCltPart();
    C7.afficherCltPart();
    C9.afficherCltPart();
    C11.afficherCltPart();*/




    return a.exec();
}
