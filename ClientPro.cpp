#include "ClientPro.h"

ClientPro::ClientPro(int Ident, string Nom, string libelle, string complement, double CodePost, string ville, string mail,
                     string SIRET, string StatJur, string LIBELLE, string COMPLEMENT, double CODEPOST, string VILLE)
    :Client(Ident,Nom,libelle,complement, CodePost,ville, mail)
{
    this->SetSIRET(SIRET);
    this->SetStatJur(StatJur);
    this->SetLIBELLE(LIBELLE);
    this->SetCOMPLEMENT(COMPLEMENT);
    this->SetCODEPOST(CODEPOST);
    this->SetVILLE(VILLE);
}

ClientPro::~ClientPro()
{
    cout << "Destruction de (ClientPro): " << GetNom() << endl;
}

void ClientPro::SetSIRET(string siret)
{
    /*//Verifie la longueur
    if (strlen(siret) > 18)
    {
        strcpy(this->SIRET, "?");
    }
    else
    {
        strcpy(this->SIRET, siret);
    }*/
    this->SIRET = siret;
}

string ClientPro::afficher(void)
{
    ostringstream oss;
    oss << Client::afficher()
        << "Num SIRET : " << GetSIRET()
        << "\nStatut Juridique : " << GetStatJur()
        << "\nLibelle_siege : " << GetLIBELLE()
        << "\nComplement__siege : " << GetCOMPLEMENT()
        << "\nCode Postal_siege : " << GetCODEPOST()
        << "\nVille_siege : " << GetVILLE()
        << endl;

    return oss.str();
}


