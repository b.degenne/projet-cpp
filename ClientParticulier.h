#ifndef CLIENTPARTICULIER_H
#define CLIENTPARTICULIER_H

#include "Client.h"

#include <iostream>
#include <string>
#include <string.h>
#include <locale>

using namespace std;

class ClientParticulier : public Client
{
    public:

        ClientParticulier(int, string, string, string, double, string, string,
                          string, string, string);
        virtual ~ClientParticulier();

        string GetdateNaissance() { return dateNaissance; }
        void SetdateNaissance(string val) { dateNaissance = val; }
        string Getprenom() { return prenom; }
        void Setprenom(string);
        string Getsexe() { return sexe; }
        void Setsexe(string val) { sexe = val; }

        virtual string afficher (void) override;


    protected:

    private:
        string prenom ="\0";
        string sexe = "F";
        string dateNaissance = "001122";
};

#endif // CLIENTPARTICULIER_H
