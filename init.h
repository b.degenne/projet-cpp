#ifndef INIT_H
#define INIT_H

#include "Client.h"

#include <QMap>



class Init
{
private:

     static QMap<int, Client *> mapClients ;

public:
    static void alimenter();
    static QMap<int, Client *> getMapClients();
};

#endif // INIT_H
