#ifndef CLIENT_H
#define CLIENT_H
#include <iostream>
#include <string>
#include <string.h>
#include <locale>
#include <QString>
#include <sstream>

using namespace std;

class Client
{
    public:
        Client(int, string, string, string, int, string, string);
        ~Client();

        int GetIdent() { return Ident; }
        void SetIdent(int val) { Ident = val; }
        string GetNom() { return Nom; }
        void SetNom(string);
        string Getlibelle() { return libelle; }
        void Setlibelle(string val) { libelle = val; }
        string Getcomplement() { return complement; }
        void Setcomplement(string val) { complement = val; }
        int GetCodePost() { return CodePost; }
        void SetCodePost(int val) { CodePost = val; }
        string Getville() { return ville; }
        void Setville(string val) { ville = val; }
        string Getmail() { return mail; }
        void Setmail(string val) { mail = val; }

        virtual string afficher(void);

    protected:

    private:
        int Ident;
        string Nom;
        string libelle;
        string complement;
        int CodePost;
        string ville;
        string mail;
};

#endif // CLIENT_H
