#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QSqlDatabase>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_boutonData_clicked();

    void on_boutonData_2_clicked();

private:
    Ui::Dialog *ui;
    QSqlDatabase db;
};

#endif // DIALOG_H
