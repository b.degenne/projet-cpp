#ifndef CLIENTPRO_H
#define CLIENTPRO_H

#include "Client.h"


class ClientPro : public Client
{
    public:
        ClientPro(int, string, string, string, double, string, string,
                  string, string, string, string, double, string);
        virtual ~ClientPro();

        string GetSIRET() { return SIRET; }
        void SetSIRET(string);
        string GetStatJur() {return StatJur;}
        void SetStatJur(string val) {StatJur = val;}
        string GetLIBELLE() { return LIBELLE; }
        void SetLIBELLE(string val) { LIBELLE = val; }
        string GetCOMPLEMENT() { return COMPLEMENT; }
        void SetCOMPLEMENT(string val) { COMPLEMENT = val; }
        double GetCODEPOST() { return CODEPOST; }
        void SetCODEPOST(double val) { CODEPOST = val; }
        string GetVILLE() { return VILLE; }
        void SetVILLE(string val) { VILLE = val; }

        virtual string afficher(void) override;

    protected:

    private:
        string SIRET;
        string StatJur;
        string LIBELLE;
        string COMPLEMENT;
        double CODEPOST;
        string VILLE;
};

#endif // CLIENTPRO_H
